(b)
Key: attributes or sets of attributes that uniquely identify an entity within an entity set.
Workshop name
Composite key: a key that has more then one attribute
Floor and room number together form the primary key for meeting room.
Total participation: every member of the entity set participates in the relationship. 
Represent this as a double line (e.g. class cannot exit without teacher)
A workshop cannot exist unless it is held in a meeting room.
Key constraint: if every entity participates in exactly one relationship 
(both a total participation and key constraint hold) (e.g. if a class is taught by one teacher)
Each workshop is held in at most one room.
Note: dotted line is a devised attribute (ie age.)

(c)
Total participation is shown using double lines between entity and relationship.
Key constraint is shown by drawing an arrow between an entity set and a relationship.

(d)
Each workshop must have an identified organizer among the conference participants. This can be shown 
using double lines.
Every participant must register for at least one workshop. Double line between attends 
and person(shown in black)

